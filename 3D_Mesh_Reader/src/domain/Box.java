package domain;

import java.util.ArrayList;

public class Box {
	
	private ArrayList<Triangle> trianglesSaved = new ArrayList<Triangle>();
	private ArrayList<Box> boxesSaved = new ArrayList<Box>();
	
	private int depth;
	private boolean devided;
	private int id;

	public Box() {
		setDepth(0);
		setDevided(false);
	}
	
	public ArrayList<Box> devide(ArrayList<Triangle> triangles, int depth) {
		setDepth(depth);
		ArrayList<Box> boxes = new ArrayList<Box>();
		if(triangles.size()<10) {
			setTriangles(triangles);
			boxes.add(this);
		}else {
			float xmin = 9999999;
			float xmax = -9999999;
			float ymin = 9999999;
			float ymax = -9999999;
			float zmin = 9999999;
			float zmax = -9999999;
			float xsum = 0;
			float ysum = 0;
			float zsum = 0;
			for(Triangle t : triangles) {
				xmin = (xmin<=t.getSmallestX() ? xmin : t.getSmallestX());
				xmax = (xmax>t.getLargestX() ? xmax : t.getLargestX());
				ymin = (ymin<=t.getSmallestY() ? ymin : t.getSmallestY());
				ymax = (ymax>t.getLargestY() ? ymax : t.getLargestY());
				zmin = (zmin<=t.getSmallestZ() ? zmin : t.getSmallestZ());
				zmax = (zmax>t.getLargestZ() ? zmax : t.getLargestZ());
				xsum += (t.getSmallestX() + t.getLargestX())/2;
				ysum += (t.getSmallestY() + t.getLargestY())/2;
				zsum += (t.getSmallestZ() + t.getLargestZ())/2;
			}
			float xavg = xsum / triangles.size();
			float yavg = ysum / triangles.size();
			float zavg = zsum / triangles.size();
			float diffX = xmax - xmin;
			float diffY = ymax - ymin;
			float diffZ = zmax - zmin;
			ArrayList<ArrayList<Triangle>> halves = new ArrayList<ArrayList<Triangle>>();
			if(Math.abs(diffX) > Math.abs(diffY) && Math.abs(diffX) > Math.abs(diffZ)) {
				halves = getHalves("x", xavg, triangles);
			}else if(Math.abs(diffY) > Math.abs(diffZ)) {
				halves = getHalves("y", yavg, triangles);
			}else {
				halves = getHalves("z", zavg, triangles);
			}
			Box box1 = new Box();
			Box box2 = new Box();
			if(halves.get(0).size() == 0 || halves.get(1).size() == 0) {
				setTriangles(triangles);
				boxes.add(this);
			}else {
				setDevided(true);
				boxes.add(this);
				boxes.addAll(box1.devide(halves.get(0), getDepth()+1));
				boxes.addAll(box2.devide(halves.get(1), getDepth()+1));
				getboxesSaved().add(box1);
				getboxesSaved().add(box2);
			}
		}
		return boxes;
	}
	
	public ArrayList<ArrayList<Triangle>> getHalves(String var, float mid, ArrayList<Triangle> list){
		ArrayList<ArrayList<Triangle>> wrapper = new ArrayList<ArrayList<Triangle>>(); 
		ArrayList<Triangle> list1 = new ArrayList<Triangle>();
		ArrayList<Triangle> list2 = new ArrayList<Triangle>();
		for(Triangle t : list) {
			switch (var) {
			case "x":
				if((t.getSmallestX() + t.getLargestX())/2 < mid) {
					list1.add(t);
				}else {
					list2.add(t);
				}
				break;
			case "y":
				if((t.getSmallestY() + t.getLargestY())/2 < mid) {
					list1.add(t);
				}else {
					list2.add(t);
				}
				break;
			case "z":
				if((t.getSmallestZ() + t.getLargestZ())/2 < mid) {
					list1.add(t);
				}else {
					list2.add(t);
				}
				break;
			default:
				break;
			}
		}
		wrapper.add(list1);
		wrapper.add(list2);
		return wrapper;
	}
	
	public ArrayList<Triangle> getTrianglesSaved() {
		return trianglesSaved;
	}

	public void setTriangles(ArrayList<Triangle> triangles) {
		this.trianglesSaved = triangles;
	}
	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public boolean isDevided() {
		return devided;
	}

	public void setDevided(boolean devided) {
		this.devided = devided;
	}

	public ArrayList<Box> getboxesSaved() {
		return boxesSaved;
	}

	public void setboxesSaved(ArrayList<Box> boxes) {
		this.boxesSaved = boxes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
