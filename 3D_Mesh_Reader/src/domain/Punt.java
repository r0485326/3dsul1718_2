package domain;

public class Punt {

	private float x;
	private float y;
	private float z;
	
	private int id;
	
	public Punt(int id, float x, float y, float z){
		setId(id);
		setX(x);
		setY(y);
		setZ(z);
	}
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		this.z = z;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
