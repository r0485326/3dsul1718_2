package domain;

public class Triangle {

	private Punt punt1;
	private Punt punt2;
	private Punt punt3;
	
	public float smallestX;
	public float smallestY;
	public float smallestZ;
	public float largestX;
	public float largestY;
	public float largestZ;

	private int id;
	
	public Triangle(){}
	
	public Triangle(int id, Punt punt1, Punt punt2, Punt punt3){
		setId(id);
		setPunt1(punt1);
		setPunt2(punt2);
		setPunt3(punt3);
	}
	
	public void calculateMinMax() {
		setSmallestX(calcSmallestX());
		setSmallestY(calcSmallestY());
		setSmallestZ(calcSmallestZ());
		setLargestX(calcLargestX());
		setLargestY(calcLargestY());
		setLargestZ(calcLargestZ());
	}
	
	public float calcLargestX() {
		if(getPunt1().getX() > getPunt2().getX() && getPunt1().getX() > getPunt3().getX()) {
			return getPunt1().getX();
		}else {
			if(getPunt2().getX() > getPunt3().getX()) {
				return getPunt2().getX();
			}else {
				return getPunt3().getX();
			}
		}
	}
	
	public float calcLargestY() {
		if(getPunt1().getY() > getPunt2().getY() && getPunt1().getY() > getPunt3().getY()) {
			return getPunt1().getY();
		}else {
			if(getPunt2().getY() > getPunt3().getY()) {
				return getPunt2().getY();
			}else {
				return getPunt3().getY();
			}
		}
	}
	
	public float calcLargestZ() {
		if(getPunt1().getZ() > getPunt2().getZ() && getPunt1().getZ() > getPunt3().getZ()) {
			return getPunt1().getZ();
		}else {
			if(getPunt2().getZ() > getPunt3().getZ()) {
				return getPunt2().getZ();
			}else {
				return getPunt3().getZ();
			}
		}
	}
	
	public float calcSmallestX() {
		if(getPunt1().getX() < getPunt2().getX() && getPunt1().getX() < getPunt3().getX()) {
			return getPunt1().getX();
		}else {
			if(getPunt2().getX() < getPunt3().getX()) {
				return getPunt2().getX();
			}else {
				return getPunt3().getX();
			}
		}
	}
	
	public float calcSmallestY() {
		if(getPunt1().getY() < getPunt2().getY() && getPunt1().getY() < getPunt3().getY()) {
			return getPunt1().getY();
		}else {
			if(getPunt2().getY() < getPunt3().getY()) {
				return getPunt2().getY();
			}else {
				return getPunt3().getY();
			}
		}
	}
	
	public float calcSmallestZ() {
		if(getPunt1().getZ() < getPunt2().getZ() && getPunt1().getZ() < getPunt3().getZ()) {
			return getPunt1().getZ();
		}else {
			if(getPunt2().getZ() < getPunt3().getZ()) {
				return getPunt2().getZ();
			}else {
				return getPunt3().getZ();
			}
		}
	}

	public Punt getPunt1() {
		return punt1;
	}

	public void setPunt1(Punt punt1) {
		this.punt1 = punt1;
	}

	public Punt getPunt2() {
		return punt2;
	}

	public void setPunt2(Punt punt2) {
		this.punt2 = punt2;
	}

	public Punt getPunt3() {
		return punt3;
	}

	public void setPunt3(Punt punt3) {
		this.punt3 = punt3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Float getSmallestX() {
		return smallestX;
	}

	public void setSmallestX(Float smallestX) {
		this.smallestX = smallestX;
	}

	public Float getSmallestY() {
		return smallestY;
	}

	public void setSmallestY(Float smallestY) {
		this.smallestY = smallestY;
	}

	public Float getSmallestZ() {
		return smallestZ;
	}

	public void setSmallestZ(Float smallestZ) {
		this.smallestZ = smallestZ;
	}

	public Float getLargestX() {
		return largestX;
	}

	public void setLargestX(Float largestX) {
		this.largestX = largestX;
	}

	public Float getLargestY() {
		return largestY;
	}

	public void setLargestY(Float largestY) {
		this.largestY = largestY;
	}

	public Float getLargestZ() {
		return largestZ;
	}

	public void setLargestZ(Float largestZ) {
		this.largestZ = largestZ;
	}
}
