package domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.TreeMap;

public class MeshTXTReader {

	private ArrayList<Punt> hoekPunten = new ArrayList<Punt>();
	private ArrayList<Triangle> triangles = new ArrayList<Triangle>();

	public MeshTXTReader() throws IOException {
		long startTotal = System.currentTimeMillis();
		readFile();
		TreeMap<Integer, ArrayList<Box>> boxes = computeBoxes();
		writeFileMap(boxes);
		System.out.println("Total time: " + (double)(System.currentTimeMillis() - startTotal) / (double)1000 + " sec");
	}

	public void readFile() throws IOException {
		long start = System.currentTimeMillis();
		String path = new File(".").getAbsolutePath();
		File file = new File(path + "/egret.txt");
		System.out.println("Start reading");
		try {
			Scanner scanner = new Scanner(file);
			String line;
			int pointCounter = 0;
			int TriangleCounter = 0;
			while (scanner.hasNext()) {
				line = scanner.nextLine();
				if (!line.trim().isEmpty()) {
					if (!line.substring(0, 1).matches(".*[a-zA-Z].*")) {	//Overslagen van de header, kijkt na of de lijn een letter bevat
						String[] meshData = line.split(" ");
						if (meshData[0].length() > 1) {
							float x = Float.parseFloat(meshData[0]);
							float y = Float.parseFloat(meshData[1]);
							float z = Float.parseFloat(meshData[2]);
							Punt punt = new Punt(pointCounter, x, y, z);
							pointCounter++;
							this.hoekPunten.add(punt);
						} else {
							int firstPoint = Integer.parseInt(meshData[1]);
							int secondPoint = Integer.parseInt(meshData[2]);
							int thirdPoint = Integer.parseInt(meshData[3]);
							Triangle triangle = new Triangle(TriangleCounter, hoekPunten.get(firstPoint),
									hoekPunten.get(secondPoint), hoekPunten.get(thirdPoint));
							TriangleCounter++;
							triangles.add(triangle);
						}
					}
				}
			}
			scanner.close();
			System.out.println("Reading done (" + (double)(System.currentTimeMillis() - start) / (double)1000 + " sec)");
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	public TreeMap<Integer, ArrayList<Box>> computeBoxes() {
		long start = System.currentTimeMillis();
		System.out.println("Start calculating");
		calcMinMax();
		System.out.println("Calculating done (" + (double)(System.currentTimeMillis() - start) / (double)1000 + " sec)");
		start = System.currentTimeMillis();
		System.out.println("Start optimising");
		Box box = new Box();
		ArrayList<Box> boxes = box.devide(triangles, 1);
		System.out.println("Optimising done (" + boxes.size() + " boxes, " + (double)(System.currentTimeMillis() - start) / (double)1000 + " sec)");
		start = System.currentTimeMillis();
		System.out.println("Start indexing boxes");
		TreeMap<Integer, ArrayList<Box>> boxesSorted = getBoxesSorted(boxes);
		System.out.println("Indexing done (" + (double)(System.currentTimeMillis() - start) / (double)1000 + " sec)");
		return boxesSorted;
	}
	
	public TreeMap<Integer, ArrayList<Box>> getBoxesSorted(ArrayList<Box> boxes){
		TreeMap<Integer, ArrayList<Box>> boxesSorted = new TreeMap<Integer, ArrayList<Box>>(Collections.reverseOrder());
		for(Box b : boxes) {
			if(!boxesSorted.containsKey(b.getDepth())) {
				boxesSorted.put(b.getDepth(), new ArrayList<Box>());
			}
			b.setId(boxesSorted.get(b.getDepth()).size()+1);
			boxesSorted.get(b.getDepth()).add(b);
		}
		return boxesSorted;
	}
	
	public void calcMinMax() {
		for(Triangle t : triangles) {
			t.calculateMinMax();
		}
	}

	public void writeFileMap(TreeMap<Integer, ArrayList<Box>> boxes) {
		long start = System.currentTimeMillis();
		String path = new File(".").getAbsolutePath();
		File file = new File(path + "/egretTriangles.txt");
		System.out.println("Start writing");
		try {
			FileWriter fileWriter = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			for (Punt p : hoekPunten) {
				printWriter.print("P " + p.getId() + " " + p.getX() + " " + p.getY() + " " + p.getZ() + "\n");
			}
			for (Triangle t : triangles) {
				printWriter.print("T " + t.getId() + " " + t.getPunt1().getId() + " " + t.getPunt2().getId() + " "
						+ t.getPunt3().getId() + "\n");
			}
			for(ArrayList<Box> boxList : boxes.values()) {
				for(Box b : boxList) {
					String result = "";
					result += ("B"+b.getDepth() + " " + b.getId()  + " " + b.isDevided() + " ");
					if(b.isDevided()) {
						for(Box b2 : b.getboxesSaved()) {
							result += (b2.getId() + " ");
						}
					}else {
						result += (b.getTrianglesSaved().size() + " ");
						for(Triangle t : b.getTrianglesSaved()) {
							result += (t.getId() + " ");
						}
					}
					printWriter.print(result);
					if(b.getDepth() > 1) {
						printWriter.print("\n");
					}
					
				}
			}
			printWriter.close();
			System.out.println("Writing done (" + (double)(System.currentTimeMillis() - start) / (double)1000 + " sec)");
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}