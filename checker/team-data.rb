	team_member 'Jordy' do
  #green:
	extension PlaneXZ
	extension PlaneXZOptimized
	extension TriangleOptimized
	extension SquareXZ
	extension SquareXZOptimized
	extension PrimitiveRotationX
  #green-orange:
	extension RayTracerV3
	extension RayTracerV4
	extension MaterialVerticalLines
	extension Triangle
  #orange:
	extension MaterialRotate2D
	extension Bounce
	extension MaterialTransformer3D
  #orange-red:
	extension ParallelScheduler
	extension BoundingBox
	extension Lissajous
	extension Quintic #TER VERVANGING VAN 1 ORANJE
  #red:
	extension MeshReading
  
end

team_member 'Boris' do
  extension RandomSampler
  extension StratifiedSampler
  extension JitteredSampler
  extension HalfJitteredSampler
  extension NRooksSampler
  extension MultijitteredSampler
  extension SpotLight
  extension SquareXY
  extension SquareXYOptimized
  extension PrimitiveRotationZ
  extension EasingLibrary
  extension MaterialTranslate2D
  extension AngleAnimation
  extension Quadratic
end

team_member 'Kevin' do 
  #green:
	extension RayTracerV1
	extension PlaneYZ
	extension PlaneYZOptimized
	extension PrimitiveRotationY
	extension PrimitiveScaling
  #green-orange:
	extension RayTracerV2
	extension MaterialGrid2d
	extension MaterialGrid3d
	extension PointAnimation
  #orange
	extension RayTracerV5
	extension MaterialTransformer2D
	extension MaterialScale2D
	extension MaterialScale3D
  #orange-red
	extension RayTracerV6
	extension Cubic
  #red
	extension MeshOptimizing
end



# Possible extensions
# You can copy paste lines below

# extension BasicSample
# extension BasicScripting
# extension RayTracerV1
# extension RayTracerV2
# extension RayTracerV3
# extension RayTracerV4
# extension RayTracerV5
# extension RayTracerV6
# extension RandomSampler
# extension StratifiedSampler
# extension JitteredSampler
# extension HalfJitteredSampler
# extension NRooksSampler
# extension MultijitteredSampler
# extension DepthOfFieldCamera
# extension FisheyeCamera
# extension OrthographicCamera
# extension DirectionalLight
# extension SpotLight
# extension AreaLight
# extension Voronoi2D
# extension Voronoi3D
# extension MaterialDalmatian2D
# extension MaterialVerticalLines
# extension MaterialGrid2d
# extension MaterialCheckered2D
# extension MaterialDalmatian3D
# extension MaterialGrid3d
# extension MaterialCheckered3D
# extension MaterialWorley2D
# extension MaterialWorley3D
# extension MaterialPerlin2D
# extension MaterialPerlin3D
# extension MaterialMarble2D
# extension MaterialMarble3D
# extension MaterialTransformer2D
# extension MaterialTransformer3D
# extension MaterialScale2D
# extension MaterialScale3D
# extension MaterialTranslate2D
# extension MaterialTranslate3D
# extension MaterialRotate2D
# extension MaterialRotate3D
# extension PlaneXZ
# extension PlaneYZ
# extension PlaneXZOptimized
# extension PlaneYZOptimized
# extension ConeAlongX
# extension ConeAlongY
# extension ConeAlongZ
# extension ConeAlongXOptimized
# extension ConeAlongYOptimized
# extension ConeAlongZOptimized
# extension SquareXY
# extension SquareXZ
# extension SquareYZ
# extension SquareXYOptimized
# extension SquareXZOptimized
# extension SquareYZOptimized
# extension Cube
# extension CylinderAlongX
# extension CylinderAlongY
# extension CylinderAlongZ
# extension CylinderAlongXOptimized
# extension CylinderAlongYOptimized
# extension CylinderAlongZOptimized
# extension Triangle
# extension TriangleOptimized
# extension BoundingBox
# extension Cropper
# extension CropperOptimized
# extension Bumpifier
# extension Intersection
# extension IntersectionOptimized
# extension Difference
# extension DifferenceOptimized
# extension PrimitiveScaling
# extension PrimitiveRotationX
# extension PrimitiveRotationY
# extension PrimitiveRotationZ
# extension Group
# extension Edge
# extension Cartoon
# extension ParallelScheduler
# extension MotionBlur
# extension Bmp
# extension Ppm
# extension EasingLibrary
# extension Bounce
# extension Elastic
# extension Quadratic
# extension Cubic
# extension Quintic
# extension PointAnimation
# extension AngleAnimation
# extension Quaternions
# extension CircularAnimation
# extension Lissajous
# extension Cyclic
# extension Slicer
# extension MeshReading
# extension MeshOptimizing
# extension MeshRendering
