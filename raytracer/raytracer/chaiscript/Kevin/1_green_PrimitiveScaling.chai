def scene_at(now)
{
  var x = Animations.animate(1.3, 0.25, seconds(5))

  var camera = Cameras.perspective( [ "eye": pos(1,1,3.9),
                                      "look_at": pos(0,0,0),
                                      "up": vec(0,1,0).normalized() ] )


  var floor_material = Materials.uniform( [ "ambient": Colors.white() * 0.1,
                                            "diffuse": Colors.white() * 0.8,
                                            "reflectivity": 0.3 ] )
                                            
  var red_wall_material = Materials.uniform( [ "ambient": Colors.red() * 0.1,
                                            "diffuse": Colors.red() * 0.8] )
                                            
  var blue_wall_material = Materials.uniform( [ "ambient": Colors.blue() * 0.1,
                                            "diffuse": Colors.blue() * 0.8 ] )
                                            
  var yellow_wall_material = Materials.uniform( [ "ambient": Colors.yellow() * 0.1,
                                            "diffuse": Colors.yellow() * 0.8 ] )

  var green_wall_material = Materials.uniform( [ "ambient": Colors.green() * 0.1,
                                                "diffuse": Colors.green() * 0.8 ] )
                                                
  var sphere_material = Materials.uniform( [ "ambient": Colors.white() * 0.1,
                                            "diffuse": Colors.white() * 0.8,
                                            "reflectivity": 0.3] )

  var ceiling_material = floor_material

  var primitives = []
  primitives.push_back( translate(vec(0,-2,0), decorate(floor_material, xz_plane())) )
  primitives.push_back( translate(vec(0,2,0), decorate(ceiling_material, xz_plane())) )
  primitives.push_back( translate(vec(0,0,-2), decorate(blue_wall_material, xy_plane())) )
  primitives.push_back( translate(vec(0,0,4), decorate(yellow_wall_material, xy_plane())) )
  primitives.push_back( translate(vec(-2,0,0), decorate(red_wall_material, yz_plane())) )
  primitives.push_back( translate(vec(2,0,0), decorate(green_wall_material, yz_plane())) )
  primitives.push_back( decorate( sphere_material, scale(x[now], x[now], x[now], sphere())) )
  
  var root = union(primitives)

  var lights = [Lights.omnidirectional( pos(0,1,2), Colors.white() ) ]
  
  create_scene(camera, root, lights)
  
}

var renderer = Renderers.standard( [ "width": 500,
                                     "height": 500,
                                     "sampler": Samplers.multijittered(2),
                                     "ray_tracer": Raytracers.v5() ] )

pipeline( scene_animation(scene_at, seconds(5)),
          [ Pipeline.animation(30),
            Pipeline.renderer(renderer),
            Pipeline.wif(),
            Pipeline.base64(),
            Pipeline.stdout() ] )