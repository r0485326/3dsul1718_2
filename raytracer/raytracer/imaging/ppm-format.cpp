#include "ppm-format.h"
#include <sstream>

void imaging::PPM::write_text_ppm(const Bitmap& bitmap, std::ostream& out)
{
	unsigned width = bitmap.width();
	unsigned height = bitmap.height();
	
	//ofstream myfile("example.txt");
	
	//HEADER
	out << "P3" << std::endl;
	//width and height
	out << width << ' ' << height<< std::endl;
	//Maximum color value
	out << 255 << std::endl;

	for (unsigned i = 0; i < height; i++)
	{
		for (unsigned j = 0; j < width; j++)
		{
			Color c = bitmap[Position2D(j, i)].clamped();
			int r = int(c.r * 255);
			int g = int(c.g * 255);
			int b = int(c.b * 255);

			out << r << ' ' << g << ' ' << ' ' << b << "\t";
		}
		out << std::endl;
	}
}
