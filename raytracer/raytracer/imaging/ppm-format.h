#pragma once
#include "imaging/bitmap.h"

namespace imaging
{
	class PPM
	{
	public: 
		PPM() {}
		void write_text_ppm(const Bitmap& bitmap, std::ostream& out);
	};
	
}