#pragma once

#include "samplers/sampler.h"


namespace raytracer
{

	namespace samplers
	{

		/// <summary>
		/// Creates a sampler that returns n points in the rectangle
		/// </summary>
		Sampler random(const int& samplecount);
	}
}
