
#pragma once

#include "samplers/sampler.h"

namespace raytracer
{

	namespace samplers
	{

		/// <summary>
		/// Creates a sampler that returns the center of all rectangles that are subdivised by the arguments given in a raster.
		/// </summary>
		Sampler stratisfied(const int& row,const int& col);
	}
}
