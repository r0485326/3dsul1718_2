#include "samplers/half_jittered_sampler.h"
#include "math/rasterizer.h"
#include <random>

using namespace math;
using namespace raytracer;

namespace
{
	class HalfJitteredSampler : public raytracer::samplers::_private_::SamplerImplementation
	{
	private:
		int rows;
		int cols;
	public:

		HalfJitteredSampler(const int& row,const int& col) {
			rows = row;
			cols = col;
		}

		std::vector<Point2D> sample(const math::Rectangle2D& rectangle) const override
		{
			std::vector<Point2D> result;
			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_real_distribution<double> dis(0, 1);

			auto rasterizer = Rasterizer(rectangle, rows, cols);
			for (int i = 0; i < rows; ++i) {
				for (int j = 0; j < cols; ++j) {
					Position2D pos = Position2D(i, j);
					Rectangle2D rec = rasterizer[pos];
					Point2D origin = rec.from_relative(Point2D(0.25,0.25));
					Vector2D xaxis = Vector2D(origin.x(), rec.from_relative(Point2D(0.75, 0.25)).x());    //HIER MOETEN DOUBLES KOMEN IPV ORIGIN DAT EEN POINT2D IS, ANDERS BUILD ERROR!
					Vector2D yaxis = Vector2D(origin.x(), rec.from_relative(Point2D(0.25, 0.75)).y());
					Rectangle2D smallrec = Rectangle2D(origin,xaxis,yaxis);
					
					double x = dis(gen);
					double y = dis(gen);

					result.push_back(rec.from_relative(smallrec.from_relative(Point2D(x, y))));
				}
			}
			return result;
		}
	};
}


Sampler raytracer::samplers::halfjittered(const int& row,const int& col)
{
	return Sampler(std::make_shared<HalfJitteredSampler>(row, col));
}