#include "samplers/multijittered_sampler.h"
#include "math/rasterizer.h"
#include <random>

using namespace math;
using namespace raytracer;

namespace
{
	class MultijitteredSampler : public raytracer::samplers::_private_::SamplerImplementation
	{
	private:
		int dim;
	public:

		MultijitteredSampler(const int& dimension) {
			dim = dimension;
		}

		std::vector<Point2D> sample(const math::Rectangle2D& rectangle) const override
		{
			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_real_distribution<double> unidis(0, 1);

			auto rasterizer1 = Rasterizer(rectangle, dim, dim);
			std::vector<Position2D> vec;
			for (int i = 0; i < dim; i++) {
				for (int j = 0; j < dim; j++) {
					vec.push_back(Position2D(i, j));
				}
			}
			std::vector<Point2D> result;
			std::shuffle(vec.begin(), vec.end(), std::default_random_engine());
			int counter = 0;
			for (int x = 0; x < dim; ++x) {
				for (int y = 0; y < dim; ++y) {
					Position2D pos = Position2D(x, y);
					Rectangle2D rec = rasterizer1[pos];
					auto rasterizer = Rasterizer(rec, dim, dim);
					Position2D minipos = vec.at(counter);
					++counter;
					Rectangle2D minrec = rasterizer[minipos];
					Point2D point = Point2D(unidis(gen), unidis(gen));
					result.push_back(minrec.from_relative(point));
				}

			}
			return result;
		}
	};
}


Sampler raytracer::samplers::multijittered(const int& dim)
{
	return Sampler(std::make_shared<MultijitteredSampler>(dim));
}