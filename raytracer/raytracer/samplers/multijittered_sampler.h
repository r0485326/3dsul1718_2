#pragma once
#include "samplers/sampler.h"

namespace raytracer
{

	namespace samplers
	{

		/// <summary>
		/// Creates a sampler that returns random points, but no too close to the edges of the subdivised rectangle according to the given rows and colums in a raster layout. 
		/// </summary>
		Sampler multijittered(const int& dim);
	}
}
