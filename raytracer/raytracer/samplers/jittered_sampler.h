#pragma once
#include "samplers/sampler.h"

namespace raytracer
{

	namespace samplers
	{

		/// <summary>
		/// Creates a sampler that returns a random point in each subdivised rectangle, according to the given rows and columns in a raster.
		/// </summary>
		Sampler jittered(const int& row,const int& col);
	}
}
