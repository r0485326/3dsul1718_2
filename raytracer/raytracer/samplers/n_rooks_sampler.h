#pragma once
#include "samplers/sampler.h"

namespace raytracer
{

	namespace samplers
	{

		/// <summary>
		/// An n-rooks sampler takes a single parameter N .It subdivides the given rectangle in
		/// N*N subrectangles.It then picks Nrandom subrectangles so that in exactly one 
		/// subrectangle is picked in each row and exactly one subrectangle is picked in each column.
		/// From each selected subrectangle, a random point is returned.
		/// </summary>
		Sampler nrooks(const int& dimension);
	}
}
