#include "samplers/n_rooks_sampler.h"
#include "math/rasterizer.h"
#include <random>

using namespace math;
using namespace raytracer;

namespace
{
	class NRooksSampler : public raytracer::samplers::_private_::SamplerImplementation
	{
	private:
		int dim;
	public:

		NRooksSampler(const int& dimension) {
			dim = dimension;
		}

		std::vector<Point2D> sample(const math::Rectangle2D& rectangle) const override
		{
			std::vector<Point2D> result;
			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_real_distribution<double> unidis(0, 1);
			std::vector<Rectangle2D> reclist;


			auto rasterizer = Rasterizer(rectangle,dim,dim);
			std::uniform_int_distribution<int> uniint(0, dim-1);
			if (dim != 1) {
				for (int y = 0; y < dim; ++y) {
					while (reclist.size() != y + 1) {
						int rand = uniint(gen);
						Position2D pos = Position2D(rand, y);
						Rectangle2D rec = rasterizer[pos];
						bool same = false;
						for (int y2 = 0; y2 < dim; ++y2) {
							if (y != y2) {
								Position2D poscheck = Position2D(rand, y2);
								Rectangle2D reccheck = rasterizer[poscheck];
								for (int i = 0; i < reclist.size(); ++i) {
									if (reclist.size() != 0 && reclist.at(i) == reccheck) {
										same = true;
									}
								}
							}
						}
						if (!same) {
							reclist.push_back(rec);
						}

					}
				}
			}
			else {
				reclist.push_back(rectangle);
			}
			for (int i = 0; i < reclist.size(); ++i) {
				Rectangle2D rec = reclist.at(i);
				double x = unidis(gen);
				double y = unidis(gen);
				result.push_back(rec.from_relative(Point2D(x, y)));
			}
			return result;
		}
	};
}


Sampler raytracer::samplers::nrooks(const int& dimension)
{
	return Sampler(std::make_shared<NRooksSampler>(dimension));
}