// Add an include for each sampler
#include "samplers/single-sampler.h"
#include "samplers/random-sampler.h"
#include "samplers/stratisfied_sampler.h"
#include "samplers/jittered_sampler.h"
#include "samplers/half_jittered_sampler.h"
#include "samplers/n_rooks_sampler.h"
#include "samplers/multijittered_sampler.h"