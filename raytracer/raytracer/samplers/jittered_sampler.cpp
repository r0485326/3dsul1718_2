#include "samplers/jittered_sampler.h"
#include "math/rasterizer.h"
#include <random>

using namespace math;
using namespace raytracer;

namespace
{
	class JitteredSampler : public raytracer::samplers::_private_::SamplerImplementation
	{
	private:
		int rows;
		int cols;
	public:

		JitteredSampler(const int& row,const int& col) {
			rows = row;
			cols = col;
		}

		std::vector<Point2D> sample(const math::Rectangle2D& rectangle) const override
		{
			std::vector<Point2D> result;
			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_real_distribution<double> dis(0, 1);

			auto rasterizer = Rasterizer(rectangle, rows, cols);
			for (int i = 0; i < rows; ++i) {
				for (int j = 0; j < cols; ++j) {
					Position2D pos = Position2D(i, j);
					Rectangle2D rec = rasterizer[pos];

					double x = dis(gen);
					double y = dis(gen);

					result.push_back(rec.from_relative(Point2D(x, y)));
				}
			}
			return result;
		}
	};
}


Sampler raytracer::samplers::jittered(const int& row,const int& col)
{
	return Sampler(std::make_shared<JitteredSampler>(row, col));
}