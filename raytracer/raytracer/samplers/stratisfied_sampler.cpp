#include "samplers/stratisfied_sampler.h"
#include "math/rasterizer.h"

using namespace math;
using namespace raytracer;

namespace
{
	class StratisfiedSampler : public raytracer::samplers::_private_::SamplerImplementation
	{
	private:
		int rows;
		int cols;
	public:

		StratisfiedSampler(const int& row,const int& col) {
			rows = row;
			cols = col;
		}

		std::vector<Point2D> sample(const math::Rectangle2D& rectangle) const override
		{
			std::vector<Point2D> result;
			auto rasterizer = Rasterizer(rectangle,rows,cols);
			for (int i = 0; i < rows; ++i) {
				for (int j = 0; j < cols; ++j) {
					Position2D pos = Position2D(i, j);
					Rectangle2D rec = rasterizer[pos];
					result.push_back(rec.center());
				}
			}
			return result;
		}
	};
}


Sampler raytracer::samplers::stratisfied(const int& row,const int& col)
{
	return Sampler(std::make_shared<StratisfiedSampler>(row,col));
}