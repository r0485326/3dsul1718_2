#include "tasks/parallel-task-scheduler.h"
#include <ppl.h>

using namespace tasks;

namespace
{
	/// <summary>
	/// Performs tasks in parallel.
	/// </summary>
	class ParallelTaskScheduler : public tasks::schedulers::_private_::TaskSchedulerImplementation
	{
	public:
		void perform(std::vector<std::shared_ptr<Task>> tasks) const
		{
			concurrency::parallel_for_each(tasks.begin(), tasks.end(), [](std::shared_ptr<Task> task) {
				task->perform();
			});
		}
	};
}



TaskScheduler tasks::schedulers::parallel()
{
	return TaskScheduler(std::make_shared<ParallelTaskScheduler>());
}