#include "animation\lissajous.h"

using namespace animation;

namespace
{
	//pi constant
	const double pi = std::atan(1.0) * 4;
}

Animation<math::Point3D> animation::lissajous(animation::Duration duration, const double& xAmp, const double& yAmp, const double& zAmp, const double& xFreq, const double& yFreq, const double& zFreq, math::Angle& xPhase, math::Angle& yPhase, math::Angle& zPhase)
{
	//Creates animation for 0 to 1

	auto double_animation = basic(0, 2 * pi, duration);

	std::function<math::Point3D(TimeStamp)> lambda = [xAmp, yAmp, zAmp, xFreq, yFreq, zFreq, xPhase, yPhase, zPhase, double_animation](TimeStamp now) -> math::Point3D {
		return math::Point3D(xAmp * math::sin(math::Angle::radians(xFreq * double_animation(now)) + xPhase), yAmp * math::sin(math::Angle::radians(yFreq * double_animation(now)) + yPhase), zAmp * math::sin(math::Angle::radians(zFreq * double_animation(now)) + zPhase));
	};

	return make_animation(math::from_lambda(lambda), duration);
}