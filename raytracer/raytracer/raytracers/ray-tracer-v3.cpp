#include "raytracers/ray-tracer-v3.h"

using namespace raytracer;


TraceResult raytracer::raytracers::_private_::RayTracerV3::trace(const Scene& scene, const Ray& ray) const
{
	Hit hit;

	// Ask the scene for the first positive hit, i.e. the closest hit in front of the eye
	// If there's a hit, find_first_positive_hit returns true and updates the hit object with information about the hit
	if (scene.root->find_first_positive_hit(ray, &hit))
	{
		// There's been a hit
		// Fill in TraceResult object with information about the trace

		// This ray tracer always returns white in case of a hit
		//Color hit_color = hit.material->at(hit.local_position).ambient;

		Color result = colors::black();
		MaterialProperties properties = hit.material->at(hit.local_position);
		result += compute_ambient(properties);
		result += process_lights(scene, properties, hit, ray);

		// The hit object contains the group id, just copy it (group ids are important for edge detection)
		unsigned group_id = hit.group_id;

		// The t-value indicates where the ray/scene intersection took place.
		// You can use ray.at(t) to find the xyz-coordinates in space.
		double t = hit.t;

		// Group all this data into a TraceResult object.
		return TraceResult(result, group_id, ray, t);
	}
	else
	{
		// The ray missed all objects in the scene
		// Return a TraceResult object representing "no hit found"
		// which is basically the same as returning black
		return TraceResult::no_hit(ray);
	}
}

Color raytracer::raytracers::_private_::RayTracerV3::process_light_ray(const Scene &scene, const MaterialProperties &properties, const Hit &hit, const Ray &ray, const LightRay &lightray) const
{
	Color result = RayTracerV2::process_light_ray(scene, properties, hit, ray, lightray);
	return result + compute_specular(properties, hit, ray, lightray);
	
}

Color raytracer::raytracers::_private_::RayTracerV3::compute_specular(const MaterialProperties &properties, const Hit &hit, const Ray &ray, const LightRay &lightray) const
{
	//if lus is performance optimalisatie. Kan voorlopig nog niet echt getest worden.
	if (properties.specular.r == 0 && properties.specular.g == 0 && properties.specular.b == 0) {
		return colors::black();
	}
	Vector3D i = (hit.position - lightray.ray.origin).normalized();
	Vector3D n = i.reflect_by(hit.normal);
	Vector3D r = i - 2 * (i.dot(n)) * n;
	Vector3D v = (ray.origin - hit.position).normalized();
	double cosa = v.dot(n);

	if (cosa > 0) {
		return lightray.color * properties.specular * pow(cosa, properties.specular_exponent);
	}
	return colors::black();
}




raytracer::RayTracer raytracer::raytracers::v3()
{
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV3>());
}