#include "raytracers/ray-tracer-v6.h"

using namespace raytracer;


TraceResult raytracer::raytracers::_private_::RayTracerV6::trace(const Scene& scene, const Ray& ray) const
{
	const double weight = 1.0;
	return trace(scene, ray, weight);
}

TraceResult raytracer::raytracers::_private_::RayTracerV6::trace(const Scene& scene, const Ray& ray, const double& weight) const
{
	if (weight <= 0.01) {
		return TraceResult::no_hit(ray);
	}
	Hit hit;

	// Ask the scene for the first positive hit, i.e. the closest hit in front of the eye
	// If there's a hit, find_first_positive_hit returns true and updates the hit object with information about the hit
	if (scene.root->find_first_positive_hit(ray, &hit))
	{
		// There's been a hit
		// Fill in TraceResult object with information about the trace

		// This ray tracer always returns white in case of a hit
		//Color hit_color = hit.material->at(hit.local_position).ambient;

		Color result = colors::black();
		MaterialProperties properties = hit.material->at(hit.local_position);
		result += compute_ambient(properties);
		result += process_lights(scene, properties, hit, ray);
		result += compute_reflective(scene, properties, hit, ray, weight);
		result += compute_refraction(scene, properties, hit, ray, weight);

		// The hit object contains the group id, just copy it (group ids are important for edge detection)
		unsigned group_id = hit.group_id;

		// The t-value indicates where the ray/scene intersection took place.
		// You can use ray.at(t) to find the xyz-coordinates in space.
		double t = hit.t;

		// Group all this data into a TraceResult object.
		return TraceResult(result, group_id, ray, t);
	}
	else
	{
		// The ray missed all objects in the scene
		// Return a TraceResult object representing "no hit found"
		// which is basically the same as returning black
		return TraceResult::no_hit(ray);
	}
}

Color raytracer::raytracers::_private_::RayTracerV6::compute_refraction(const Scene &scene, const MaterialProperties &props, const Hit &hit, const Ray &ray, const double weight) const
{
	if (props.transparency <= 0) {
		return colors::black();
	}

	else {
		const double n1 = 1;
		const double n2 = props.refractive_index;
		const math::Vector3D norm = hit.normal;
		const math::Vector3D dir = ray.direction;

		/*
		const int dotProd = norm.x*ray.direction.x + norm.y*ray.direction.y + norm.z*ray.direction.z; //http://www.analyzemath.com/stepbystep_mathworksheets/vectors/vector3D_angle.html
		const int mag1 = sqrt(pow(norm.x, 2) + pow(norm.y, 2) + pow(norm.z, 2));
		const int mag2 = sqrt(pow(dir.x, 2) + pow(dir.y, 2) + pow(dir.z, 2));
		const double theta = acos(dotProd / (mag1 * mag2));

		if ((n2 / n1)*sin(theta) > 1) {
			return colors::black();
		}
		else {*/


		
		const math::Vector3D oX = (n1 / n2) * (dir.dot(norm) * norm);;
		const math::Vector3D oY = (-1 * sqrt(1 - oX.dot(oX))) * norm;
		const math::Vector3D o = oX + oY;
		if (1 - oX.dot(oX) < 0){
			return colors::black();
		}

		const Point3D innerPoint = hit.position + o * 0.0000001;
		const Ray* innerRay = new Ray(innerPoint, o);

		Hit exitHit;
		
		if (scene.root->find_first_positive_hit(*innerRay, &exitHit)) {
			return trace(scene, *innerRay, weight).color;
		}
		else {
			return colors::black();
		}
			
		//}
		return colors::black();
	}

}

raytracer::RayTracer raytracer::raytracers::v6()
{
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV6>());
}