#include "raytracers/ray-tracer-v5.h"

using namespace raytracer;


TraceResult raytracer::raytracers::_private_::RayTracerV5::trace(const Scene& scene, const Ray& ray) const
{
	const double weight = 1.0;
	return trace(scene, ray, weight);
}

TraceResult raytracer::raytracers::_private_::RayTracerV5::trace(const Scene& scene, const Ray& ray, const double& weight) const
{
	if (weight <= 0.01) {
		return TraceResult::no_hit(ray);
	}
	Hit hit;

	// Ask the scene for the first positive hit, i.e. the closest hit in front of the eye
	// If there's a hit, find_first_positive_hit returns true and updates the hit object with information about the hit
	if (scene.root->find_first_positive_hit(ray, &hit))
	{
		// There's been a hit
		// Fill in TraceResult object with information about the trace

		// This ray tracer always returns white in case of a hit
		//Color hit_color = hit.material->at(hit.local_position).ambient;

		Color result = colors::black();
		MaterialProperties properties = hit.material->at(hit.local_position);
		result += compute_ambient(properties);
		result += process_lights(scene, properties, hit, ray);
		result += compute_reflective(scene, properties, hit, ray, weight);

		// The hit object contains the group id, just copy it (group ids are important for edge detection)
		unsigned group_id = hit.group_id;

		// The t-value indicates where the ray/scene intersection took place.
		// You can use ray.at(t) to find the xyz-coordinates in space.
		double t = hit.t;

		// Group all this data into a TraceResult object.
		return TraceResult(result, group_id, ray, t);
	}
	else
	{
		// The ray missed all objects in the scene
		// Return a TraceResult object representing "no hit found"
		// which is basically the same as returning black
		return TraceResult::no_hit(ray);
	}
}

Color raytracer::raytracers::_private_::RayTracerV5::compute_reflective(const Scene &scene, const MaterialProperties &props, const Hit &hit, const Ray &ray, const double weight) const
{
	if (props.reflectivity == 0.0) {
		return colors::black();
	}
	else {
		const math::Vector3D t = hit.normal.normalized();
		const math::Vector3D dir2 = ray.direction.reflect_by(t);
		const Point3D point2 = hit.position + dir2 * 0.0000001;
		const Ray* ray2 = new Ray(point2, dir2);
		return props.reflectivity * trace(scene, *ray2, weight*props.reflectivity).color;
	}
}

raytracer::RayTracer raytracer::raytracers::v5()
{
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV5>());
}