#pragma once

#include "raytracers/ray-tracer.h"
#include "raytracers/ray-tracer-v5.h"
#include <memory>

using namespace imaging;
using namespace math;

namespace raytracer
{
	namespace raytracers
	{
		namespace _private_
		{
			class RayTracerV6 : public RayTracerV5
			{
			public:
				TraceResult trace(const Scene&, const math::Ray&) const override;
				TraceResult trace(const Scene&, const math::Ray&, const double&) const;
				Color compute_refraction(const Scene&, const MaterialProperties&, const Hit&, const Ray&, const double) const;
			};
		}

		/// <summary>
		/// Creates simplest ray tracer.
		/// </summary>
		RayTracer v6();
	}
}