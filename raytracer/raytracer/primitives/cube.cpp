#include "primitives/cube.h"
#include "math/interval.h"
#include "primitives/square-primitive.h"
#include "primitives.h"
#include "primitives\union-primitive.h"

using namespace raytracer;
using namespace raytracer::primitives;
using namespace math;


namespace
{

	class CubeImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{
	private:

		void initialize_hit(Hit* hit, const Ray& ray, double t) const
		{
			hit->t = t;
			hit->position = ray.at(hit->t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.y(), hit->position.z());
			hit->normal = ray.origin.x() > 0 ? m_normal : -m_normal;
		}

	public:

		const Vector3D m_normal;

		Primitive xy_square_1 = translate(Vector3D(0, 0, -1), raytracer::primitives::xy_square());
		Primitive xy_square_2 = translate(Vector3D(0, 0, 1),raytracer::primitives::xy_square());
		Primitive xz_square_1 = translate(Vector3D(0, -1, 0), raytracer::primitives::xz_square());
		Primitive xz_square_2 = translate(Vector3D(0, 1, 0), raytracer::primitives::xz_square());
		Primitive yz_square_1 = translate(Vector3D(-1, 0, 0), raytracer::primitives::yz_square());
		Primitive yz_square_2 = translate(Vector3D(1, 0, 0), raytracer::primitives::yz_square());


		CubeImplementation() {
		}

		math::Box bounding_box() const override
		{
			std::vector<Primitive> list;
			list.push_back(xy_square_1);
			list.push_back(xy_square_2);
			list.push_back(xz_square_1);
			list.push_back(xz_square_2);
			list.push_back(yz_square_1);
			list.push_back(yz_square_2);
			Primitive all = make_union(list);
			return all->bounding_box();
		}
		

		std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
		{
			std::vector<Primitive> list;
			list.push_back(xy_square_1);
			list.push_back(xy_square_2);
			list.push_back(xz_square_1);
			list.push_back(xz_square_2);
			list.push_back(yz_square_1);
			list.push_back(yz_square_2);
			Primitive all = make_union(list);
			return all->find_all_hits(ray);
		}

		bool find_first_positive_hit(const math::Ray& ray, Hit* hit) const override
		{

			std::vector<Primitive> list;
			list.push_back(xy_square_1);
			list.push_back(xy_square_2);
			list.push_back(xz_square_1);
			list.push_back(xz_square_2);
			list.push_back(yz_square_1);
			list.push_back(yz_square_2);
			Primitive all = make_union(list);
			return all->find_first_positive_hit(ray, hit);
		}

	};
}

Primitive raytracer::primitives::cube()
{
	return Primitive(std::make_shared<CubeImplementation>());
}