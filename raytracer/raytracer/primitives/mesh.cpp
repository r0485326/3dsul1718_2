#include "primitives/mesh.h"
#include "util/misc.h"
#include "math/coordinate-systems.h"
#include "math/quadratic_equation.h"
#include "performance/performance.h"
#include "primitives/triangle-primitive.h"
#include <assert.h>
#include <map>
#include <fstream>
#include <sstream>
#include "primitives/union-primitive.h"
#include "primitives/bounding-box-accelerator.h"
using namespace raytracer;
using namespace math;
namespace raytracer {
	namespace primitives {
		Primitive readMesh() {
			std::map<int, Point3D> pointMap;
			std::map<int, Primitive> triangleMap;
			std::map<std::string, Primitive> boundingboxmap;
			std::vector<Primitive> tvector;
			std::vector<Primitive> bvector;
			std::ifstream file("C:/Users/Boris/Documents/UC Leuven-Limburg/2017-2018/3D/repository/3dsul1718/3dsul1718_2/3D_Mesh_Reader/teapot.txt");
				if (!file) {
					std::cerr << "File not found";
					abort();
				}
			while (!file.eof()) {
				std::string line;
				std::getline(file, line);
				std::istringstream iss(line);
				std::string type;
				iss >> type;
				if (type == "") {
					std::cerr << "the line was empty!";
				}
				if (type == "P") {
					int i;
					double x, y, z;
					iss >> i;
					iss >> x;
					iss >> y;
					iss >> z;
					pointMap[i] = Point3D(x, y, z);
				}
				if (type == "T") {
					int i, p1, p2, p3;
					iss >> i;
					iss >> p1;
					iss >> p2;
					iss >> p3;
					Primitive triangle =raytracer::primitives::triangle(pointMap[p1], pointMap[p2], pointMap[p3]);
					tvector.push_back(triangle);
				}
				if (type != "P"&&type != "T") {
					
				}
			}
			//std::cerr << "ok ";
			//std::cerr << boundingboxmap.size() + " ";
			//std::wcerr << bvector.size();
			return bounding_box_accelerator(make_union(tvector));
		}
	}
}






