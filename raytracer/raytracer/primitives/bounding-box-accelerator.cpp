#include "primitives/bounding-box-accelerator.h"
#include "util/misc.h"
#include "math/coordinate-systems.h"
#include "math/quadratic_equation.h"
#include "performance/performance.h"
#include <assert.h>


using namespace raytracer;
using namespace math;

namespace raytracer
{
	namespace primitives
	{
		namespace _private_
		{
			class BoundingBoxAccelerator : public raytracer::primitives::_private_::PrimitiveImplementation
			{
			public:
				Primitive primitive;
				Box childBoundingBox;

				BoundingBoxAccelerator(const Primitive &primitive) : primitive(primitive), childBoundingBox(primitive->bounding_box()) {};


				std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
				{
					//Check if the ray intersects with the box. If not just return.
					if (!childBoundingBox.is_hit_positively_by(ray)) 
					{
						return std::vector<std::shared_ptr<Hit>>();
					}
					//If it does intersect, find all the hits.
					return primitive->find_all_hits(ray);
				}

				bool find_first_positive_hit(const Ray& ray, Hit* hit) const override
				{
					if (!childBoundingBox.is_hit_positively_by(ray))
					{
						return false;
					}
					return primitive->find_first_positive_hit(ray, hit);
				}

				math::Box bounding_box() const override {
					return this->childBoundingBox;
				}


			};
		}
	}
}


Primitive raytracer::primitives::bounding_box_accelerator(const Primitive &primitive)
{
	return Primitive(std::make_shared<raytracer::primitives::_private_::BoundingBoxAccelerator>(primitive));
}

