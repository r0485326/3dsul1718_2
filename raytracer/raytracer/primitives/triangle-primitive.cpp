#include "primitives/triangle-primitive.h"
#include "util/misc.h"
#include "math/coordinate-systems.h"
#include "math/quadratic_equation.h"
#include "performance/performance.h"
#include <assert.h>

using namespace raytracer;
using namespace raytracer::primitives;
using namespace math;


namespace
{
	class TriangleImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{
	public:
		Point3D x;
		Point3D y;
		Point3D z;
		Vector3D nVector;

		TriangleImplementation(const Point3D &x, const Point3D &y, const Point3D &z) {
			this->x = x;
			this->y = y;
			this->z = z;
			nVector = (y - x).cross(z - x).normalized();
		}
		
		bool find_first_positive_hit(const Ray& ray, Hit* hit) const override
		{
			// Find smallest positive t-value 
			/*if (t > 0)
			{
				// Check that our new t is better than the pre-existing t
				if (t < hit->t)
				{
					initialize_hit(hit, ray, t);

					return true;
				}
			}*/

			// Triangle formulae
			auto t = (x - ray.origin).dot(nVector) / ray.direction.dot(nVector);

			if (t < 0 || t >= hit->t)
			{
				return false;
			}

			auto h = ray.origin + ray.direction * t;
			if ((y - x).cross(h - x).dot(nVector) < 0) {
				return false;
			}
			else if ((z - y).cross(h - y).dot(nVector) < 0)
			{
				return false;
			}
			else if ((x - z).cross(h - z).dot(nVector) < 0)
			{
				return false;
			}

			initialize_hit(hit, ray, t);
			return true;
		}


		std::vector<std::shared_ptr<Hit>> find_all_hits(const Ray& ray) const override
		{
			// Allocate vector on stack
			std::vector<std::shared_ptr<Hit>> hits;

			// Triangle formulae
			auto t = (x - ray.origin).dot(nVector) / ray.direction.dot(nVector);
			auto h = ray.origin + ray.direction * t;
			if ((y - x).cross(h - x).dot(nVector) < 0) {
				return hits;
			}
			else if ((z - y).cross(h - y).dot(nVector) < 0)
			{
				return hits;
			}
			else if ((x - z).cross(h - z).dot(nVector) < 0)
			{
				return hits;
			}

			// Allocate two Hit objects on heap and store address in shared pointers
			auto hit = std::make_shared<Hit>();

			// Initialize both hits
			initialize_hit(hit.get(), ray, t);

			// Put hits in vector
			hits.push_back(hit);

			// Return hit list
			return hits;	
		}

		math::Box bounding_box() const override
		{
			/*
			// Create a [-1, 1] x [-1, 1] x [-1, 1] box.
			auto range = interval(-1.0, 1.0);

			return Box(range, range, range);*/

			double x_min = std::min({ x.x(), y.x(),z.x() });
			double x_max = std::max({ x.x(), y.x(),z.x() });

			double y_min = std::min({ x.x(), y.x(),z.x() });
			double y_max = std::max({ x.x(), y.x(),z.x() });

			double z_min = std::min({ x.x(), y.x(),z.x() });
			double z_max = std::max({ x.x(), y.x(),z.x() });

			auto x = interval(x_min, x_max);
			auto y = interval(y_min, y_max);
			auto z = interval(z_min, z_max);

			return Box(x, y, z);
		}

	private:

		void initialize_hit(Hit* hit, const Ray& ray, double t) const
		{
			// Update Hit object
			hit->t = t;
			hit->position = ray.at(t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(0, 0);
			hit->normal = nVector;
			//hit->local_position.uv = compute_uv_from_xyz(hit->position);
			//hit->normal = compute_normal_at(ray, hit->position);

			//assert(is_on_sphere(hit->position));
		}
	};
}

Primitive raytracer::primitives::triangle(const Point3D &x, const Point3D &y, const Point3D &z)
{
	return Primitive(std::make_shared<TriangleImplementation>(x,y,z));
}
