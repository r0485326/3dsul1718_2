#ifndef EXCLUDE_SCRIPTING

#include "scripting/samplers-module.h"
#include "samplers/samplers.h"
#include "scripting/scripting-util.h"

using namespace chaiscript;
using namespace raytracer;
using namespace math;


namespace
{
    struct SamplerLibrary
    {
        Sampler single() const
        {
            return samplers::single();
        }
		Sampler random(const int& samplecount) const
		{
			return samplers::random(samplecount);
		}
		Sampler stratisfied(const int& row, const int& col) const
		{
			return samplers::stratisfied(row, col);
		}
		Sampler jittered(const int& row,const int& col) const
		{
			return samplers::jittered(row, col);
		}
		Sampler halfjittered(const int& row,const int& col) const
		{
			return samplers::halfjittered(row, col);
		}
		Sampler nrooks(const int& dim) const
		{
			return samplers::nrooks(dim);
		}
		Sampler multijittered(const int& dim) const
		{
			return samplers::multijittered(dim);
		}
    };
}

ModulePtr raytracer::scripting::_private_::create_samplers_module()
{
    auto module = std::make_shared<chaiscript::Module>();

    util::register_type<Sampler>(*module, "Sampler");

    auto sampler_library = std::make_shared<SamplerLibrary>();
    module->add_global_const(chaiscript::const_var(sampler_library), "Samplers");

#   define BIND(NAME)  module->add(fun(&SamplerLibrary::NAME), #NAME)
    BIND(single);
	BIND(random);
	BIND(stratisfied);
	BIND(jittered);
	BIND(halfjittered);
	BIND(nrooks);
	BIND(multijittered);
#   undef BIND

    return module;
}

#endif
