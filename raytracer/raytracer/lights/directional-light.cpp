#include "lights/directional-light.h"

using namespace raytracer;
using namespace math;

namespace {
	class DirectionalLight : raytracer::lights::_private_::LightSourceImplementation
	{
	public:
		DirectionalLight(const math::Vector3D& d, const imaging::Color& c) {
			direction = d;
			color = c;
		}
		std::vector<LightRay> raytracer::lights::_private_::LightSourceImplementation::lightrays_to(const math::Point3D& p) override {
			
		}
	private:
		math::Vector3D direction;
		imaging::Color color;
	};

}