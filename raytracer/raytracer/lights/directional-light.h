#pragma once
#include "lights/light-source.h"
#include "math/vector.h"
#include "math/point.h"
#include <memory>

namespace raytracer
{
	namespace lights {
		LightSource directional(const math::Vector3D& position, const imaging::Color& color);
	}
}
