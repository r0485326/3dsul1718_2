#include "lights/spot-light.h"

using namespace math;
using namespace raytracer;

namespace
{
	class SpotLight : public raytracer::lights::_private_::LightSourceImplementation
	{
	public:
		SpotLight(const math::Point3D& position,const math::Vector3D& direction,const math::Angle& angle, const imaging::Color& color)
			: m_position(position),m_direction(direction),m_angle(angle),m_color(color) { }

	protected:
		std::vector<LightRay> lightrays_to(const math::Point3D& point) const override
		{
			double cos_a = (point - this->m_position).normalized().dot(this->m_direction);
			double cos_b = math::cos(this->m_angle / 2);
			if (cos_a >= cos_b)
			{
				return std::vector<LightRay> {LightRay(math::Ray(m_position, point), m_color)};
			}
			else
			{
				return std::vector<LightRay> {};
			}
		}

	private:
		imaging::Color m_color;
		math::Point3D m_position;
		math::Vector3D m_direction;
		math::Angle m_angle;
	};
}

LightSource raytracer::lights::spot(const math::Point3D& position, const math::Vector3D& direction, const math::Angle& angle, const imaging::Color& color)
{
	return LightSource(std::make_shared<SpotLight>(position,direction,angle, color));
}
