#pragma once

#include "math/function.h"
#include "math/point.h"


namespace math
{
	namespace functions
	{
		math::Function<bool(const Point3D&)> lines_x_3d(double thickness = 0.1);
		math::Function<bool(const Point3D&)> lines_y_3d(double thickness = 0.1);
		math::Function<bool(const Point3D&)> lines_z_3d(double thickness = 0.1);
	}
}