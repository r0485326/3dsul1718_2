#include "math/functions/3D-functions.h"
#include "math/functions.h"
#include <cmath>

using namespace math;

Function<bool(const Point3D&)> math::functions::lines_x_3d(double thickness)
{
	std::function<bool(const Point3D&)> function = [thickness](const Point3D& p)
	{
		auto x = p.x();

		return std::abs(x - round(x)) < thickness / 2;
	};

	return from_lambda<bool, const Point3D&>(function);
}

Function<bool(const Point3D&)> math::functions::lines_y_3d(double thickness)
{
	std::function<bool(const Point3D&)> function = [thickness](const Point3D& p)
	{
		auto y = p.y();

		return std::abs(y - round(y)) < thickness / 2;
	};

	return from_lambda<bool, const Point3D&>(function);
}

Function<bool(const Point3D&)> math::functions::lines_z_3d(double thickness)
{
	std::function<bool(const Point3D&)> function = [thickness](const Point3D& p)
	{
		auto z = p.z();

		return std::abs(z - round(z)) < thickness / 2;
	};

	return from_lambda<bool, const Point3D&>(function);
}
