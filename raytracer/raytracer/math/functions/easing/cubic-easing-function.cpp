#include "math/functions/easing/cubic-easing-function.h"
#include "math/functions.h"
#include "math/quadratic_equation.h"

using namespace math;
using namespace math::functions;

EasingFunction math::functions::easing::cubic_in()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));
		return (1.3481*t*t*t) - (0.5095*t*t) + (0.1575*t) - 0.00007;
	};

	return from_lambda(lambda);
}

EasingFunction math::functions::easing::cubic_out()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));
		return (1.3481*t*t*t) - (3.5347*t*t) + (3.1828*t) + 0.004;
	};

	return from_lambda(lambda);
}

EasingFunction math::functions::easing::cubic_inout()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));

		if (t < 0.5) {
			return (5.3923*t*t*t) - (1.019*t*t) + (0.1575*t) - 0.00004;
		}
		else {
			return (5.3923*t*t*t) - (15.158*t*t) + (14.297*t) - 3.5308;
		}
	};

	return from_lambda(lambda);
}