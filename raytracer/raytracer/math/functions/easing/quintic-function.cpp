#include "math/functions/easing/quintic-function.h"
#include "math/functions.h"

using namespace math;
using namespace math::functions;

EasingFunction math::functions::easing::quintic_in()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));
		//y = 1,2821x5 - 0,6498x4 + 0,4633x3 - 0,0905x2 - 0,0036x + 0,0003
		return (1.2821*(t*t*t*t*t)) - (0.6498*(t*t*t*t)) + (0.4633*(t*t*t)) - (0.0905*(t*t)) - (0.0036*(t)) + 0.0003;
	};

	return from_lambda(lambda);
}

EasingFunction math::functions::easing::quintic_out()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));
		//y = 1,2821x5 - 5,7605x4 + 10,685x3 - 10,221x2 + 5,0165x - 0,0018
		return (1.2821 * (t*t*t*t*t)) - (5.7605 * (t*t*t*t)) + (10.685 * (t*t*t)) - (10.221*(t*t)) + (5.0165 * (t)) - 0.0018;
	};

	return from_lambda(lambda);
}

EasingFunction math::functions::easing::quintic_inout()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));

		if (t < 0.5) {
			//y = 20,513x5 - 5,1981x4 + 1,8531x3 - 0,1809x2 - 0,0036x + 0,0002
			auto result = (20.513 * (t*t*t*t*t)) - (5.1981 * (t*t*t*t)) + (1.853 * (t*t*t)) - (0.1809 * (t*t)) - (0.0036 * (t)) + 0.0002;
			if (result < 0) {
				return 0.0;
			}
			if (result > 1) {
				return 1.0;
			}
			return (20.513 * (t*t*t*t*t)) - (5.1981 * (t*t*t*t)) + (1.853 * (t*t*t)) - (0.1809 * (t*t)) - (0.0036 * (t)) + 0.0002;
		}
		else {
			//y = 20,513x5 - 97,366x4 + 186,19x3 - 179,32x2 + 86,966x - 15,983
			return (20.513 * (t*t*t*t*t)) - (97.366 * (t*t*t*t)) + (186.19 * (t*t*t)) - (179.32 * (t*t)) + (86.966 * (t)) - 15.983;
		}
	};

	return from_lambda(lambda);
}