#include "math/functions/easing/quadratic-function.h"
#include "math/functions.h"
#include "math/quadratic_equation.h"

using namespace math;
using namespace math::functions;

EasingFunction math::functions::easing::quadratic_in()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));

		return (0.9492*(t*t)) + (0.0444*t) + 0.0006;
	};

	return from_lambda(lambda);
}

EasingFunction math::functions::easing::quadratic_out()
{
	std::function<double(double)> lambda = [](double t) {
		assert(interval(0.0, 1.0).contains(t));

		return (-0.9492*(t*t)) + (1.9428*t) + 0.0059;
	};

	return from_lambda(lambda);
}

EasingFunction math::functions::easing::quadratic_inout()
{
	    std::function<double(double)> lambda = [](double t) {
        assert(interval(0.0, 1.0).contains(t));

		if (t < 0.5) {
			
			return (1.8984*(t*t)) + (0.0444*t) + 0.0003;
		}
		else {
			
			return (-1.8984*(t*t)) + (3.8412*t) - 0.943;
		}
    };

    return from_lambda(lambda);
}