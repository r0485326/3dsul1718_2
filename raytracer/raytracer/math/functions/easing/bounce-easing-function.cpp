#include "math/functions/easing/bounce-easing-function.h"
#include "math/interval.h"
#include "math/angle.h"
#include <assert.h>

using namespace math;
using namespace math::functions;


EasingFunction math::functions::easing::bounce(const int &numberOfBounces, const int &bounceAbsorption)
{
	std::function<double(double)> lambda = [=](double t) {
		assert(interval(0.0, 1.0).contains(t));
		return 1 - (abs(cos((2 * numberOfBounces - 1) * 180_degrees / 2 * t)) / pow(t + 1, bounceAbsorption));
	};

	return from_lambda(lambda);
}
