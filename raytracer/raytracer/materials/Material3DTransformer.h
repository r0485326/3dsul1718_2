#pragma once

#include "materials/material.h"
#include "math/transformation3d.h"

namespace raytracer
{
	namespace materials
	{
		Material translate3D(const math::Vector3D&, const Material&);
		Material transform3D(const Material& material, const math::Transformation3D& transformation3D);
		Material rotate3D(const math::Angle, const Material&);
		Material scale3D(const math::Vector3D v, const Material&);
	}
}