#include "Material2DTransformer.h"
#include "math\transformation-matrices.h"

using namespace math;
using namespace raytracer;
using namespace raytracer::materials;

class Material2DTransformer : public raytracer::materials::_private_::MaterialImplementation {
private:
	Material material;
	Transformation2D transformation2D;
public:
	Material2DTransformer(const Material& material, const Transformation2D& transformation2D): material(material), transformation2D(transformation2D) {

	}

	MaterialProperties at(const HitPosition& pos) const override
	{
		//	HitPosition pp = HitPosition(pos.uv.coord.operator*this->transformation2D.inverse_transformation_matrix);

			//return material->at(pp );
		HitPosition h = HitPosition();
		h.uv = this->transformation2D.inverse_transformation_matrix* pos.uv;
		return material->at(h);

	}

};

Material raytracer::materials::scale(const Vector2D v, const Material& material) {
	Transformation2D transformation = Transformation2D(transformation_matrices::scaling(v.x(),v.y()), transformation_matrices::scaling(-v.x(), -v.y()));
	return Material(std::make_shared<Material2DTransformer>(material, transformation));
}

Material raytracer::materials::translate(const Vector2D& v,const Material& material) {
	Transformation2D transformation = Transformation2D(transformation_matrices::translation(v), transformation_matrices::translation(-v));
	return Material(std::make_shared<Material2DTransformer>(material, transformation));
}

Material raytracer::materials::transform(const Material& material, const Transformation2D& transformation2D)
{
	return Material(std::make_shared<Material2DTransformer>(material, transformation2D));
}

Material raytracer::materials::rotate(const Angle angle, const Material& material)
{
	Transformation2D transformation = Transformation2D(transformation_matrices::rotation(angle), transformation_matrices::rotation(-angle));
	return Material(std::make_shared<Material2DTransformer>(material, transformation));
}