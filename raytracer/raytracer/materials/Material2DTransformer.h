#pragma once

#include "materials/material.h"
#include "math/transformation2d.h"

namespace raytracer
{
	namespace materials
	{
		Material translate(const math::Vector2D&, const Material&);
		Material transform(const Material& material, const math::Transformation2D& transformation2D);
		Material rotate(const math::Angle, const Material&);
		Material scale(const math::Vector2D v, const Material&);
	}
}