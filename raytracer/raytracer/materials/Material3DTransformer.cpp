#include "Material3DTransformer.h"
#include "math\transformation-matrices.h"

using namespace math;
using namespace raytracer;
using namespace raytracer::materials;

class Material3DTransformer : public raytracer::materials::_private_::MaterialImplementation {
private:
	Material material;
	Transformation3D transformation3D;
public:
	Material3DTransformer(const Material& material, const Transformation3D& transformation3D): material(material), transformation3D(transformation3D) {

	}

	MaterialProperties at(const HitPosition& pos) const override
	{
		//	HitPosition pp = HitPosition(pos.uv.coord.operator*this->transformation2D.inverse_transformation_matrix);

		//return material->at(pp );
		HitPosition h = HitPosition();
		h.xyz = this->transformation3D.inverse_transformation_matrix* pos.xyz;
		return material->at(h);

	}
};

Material raytracer::materials::scale3D(const math::Vector3D v, const Material& material) {
	//Transformation3D transformation = Transformation3D(transformation_matrices::scaling(v.x(), v.y(), v.z()), transformation_matrices::scaling(-v.x(), -v.y(), -v.z()));
	//return Material(std::make_shared<Material3DTransformer>(material, transformation));

	Transformation3D transformation = Transformation3D(transformation_matrices::scaling(v.x(), v.y(), v.z()), transformation_matrices::scaling(-v.x(), -v.y(), -v.z()));
	return Material(std::make_shared<Material3DTransformer>(material, transformation));
	return material;
}

Material raytracer::materials::translate3D(const Vector3D& v, const Material& material) {
	Transformation3D transformation = Transformation3D(transformation_matrices::translation(v), transformation_matrices::translation(-v));
	return Material(std::make_shared<Material3DTransformer>(material, transformation));
}

Material raytracer::materials::transform3D(const Material& material, const Transformation3D& transformation3D)
{
	return Material(std::make_shared<Material3DTransformer>(material, transformation3D));
}

Material raytracer::materials::rotate3D(const Angle angle, const Material& material)
{
	Transformation3D transformation = Transformation3D(transformation_matrices::rotation_around_x(angle), transformation_matrices::rotation_around_y(angle));
	return Material(std::make_shared<Material3DTransformer>(material, transformation));
}